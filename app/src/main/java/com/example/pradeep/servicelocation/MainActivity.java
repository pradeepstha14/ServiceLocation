package com.example.pradeep.servicelocation;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
    Button btnStart, btnStop;
    TextView tvCoordinates;
    private BroadcastReceiver broadcastReceiver;
    GoogleMap mMap;
    MapView mapView;
    ArrayList<LatLng> locations, fake_location;
//    Double[]  flattitude;
//    Double[] flongitude;


    @Override
    protected void onResume() {
        super.onResume();
        if (broadcastReceiver == null) {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    double lattitude = (double) intent.getExtras().get("lat");
                    double longitude = (double) intent.getExtras().get("longi");

                    locations.add(new LatLng(lattitude, longitude));

                    //tvCoordinates.append("\n" + intent.getExtras().get("coordinates"));

                        for (int i = 0; i < locations.size(); i++) {


                            mMap.addMarker(new MarkerOptions().position(locations.get(i)));
                            tvCoordinates.setText("\n" + locations.get(i));
                            if (i > 0) {

                                PolylineOptions poptiion = new PolylineOptions().add(locations.get(i-1)).add(locations.get(i)).width(5).color(Color.BLUE).geodesic(true);
                                mMap.addPolyline(poptiion);

                            }




                    }




                }
            };
        }
        registerReceiver(broadcastReceiver, new IntentFilter("location_update"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        locations = new ArrayList();
        fake_location = new ArrayList();
        mapView = (MapView) findViewById(R.id.map);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }


        btnStart = (Button) findViewById(R.id.id_startButton);
        btnStop = (Button) findViewById(R.id.id_stopButton);
        tvCoordinates = (TextView) findViewById(R.id.id_tvcoordinates);
        runtime_permissions();
        if (!runtime_permissions())
            enable_buttons();





    }

    private void enable_buttons() {

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), GPS_Service.class);
                startService(i);
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(), GPS_Service.class);
                stopService(i);

            }
        });

    }

    private boolean runtime_permissions() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);

            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if (requestCode == 100) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    enable_buttons();
                } else {
                    runtime_permissions();
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng nepal = new LatLng(27.700769, 85.300140);
        mMap.addMarker(new MarkerOptions().position(nepal).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(nepal));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(nepal, 5));


//        flattitude = new Double[]{27.6710, 27.7172, 27.6644, 27.6332, 27.6276};
//        flongitude = new Double[]{85.4298, 85.3240, 85.3188, 85.5277, 86.2260};
//        for (int x = 0; x < 5; x++) {
//
//            fake_location.add(new LatLng(flattitude[x], flongitude[x]));
//
//        }



//        try {
//            for (int f = 0; f < fake_location.size(); f++) {
//
//                if (f > 0) {
//                    int c = 0;
//                    PolylineOptions poptiion = new PolylineOptions().add(fake_location.get(c)).add(fake_location.get(f)).width(5).color(Color.BLUE).geodesic(true);
//                    mMap.addPolyline(poptiion);
//                    c = c + 1;
//                }
//
//
//            }
//        } catch (NullPointerException e) {
//
//        }

//live



    }
}

